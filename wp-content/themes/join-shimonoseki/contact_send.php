<?php

if ($_POST['upflg'] == 1) {

    mb_language("Japanese");
    mb_internal_encoding("UTF-8");

    $mail_title = "Joinしよう！運営事務局";
    $mailto   = "join@tmr-inc.jp";
    $site_url_mail = "https://join-shimonoseki.com/";
    $header_bcc = "join@tmr-inc.jp,hiraoka@yujihiraoka.com";
    $subject = "Joinしよう！運営事務局でございます。";

    $message = <<<_message
{$_POST['uname']}

Joinしよう！運営事務局でございます。

お問い合わせありがとうございます。

以下の内容で受け付けました。
-----------------------------------------
イベント：
【学生】
{$_POST['checkboxGroupOne'][0]}
{$_POST['checkboxGroupOne'][1]}
【企業】
{$_POST['checkboxGroupTwo'][0]}
{$_POST['checkboxGroupTwo'][1]}
お名前： {$_POST['uname']}
生年月日： {$_POST['date-year']}-{$_POST['date-month']}-{$_POST['date-day']}
メールアドレス： {$_POST['email']}
お電話番号： {$_POST['phone']}
内容： {$_POST['message']}
-----------------------------------------

通常1～2営業日以内にご返信させていただきます。
ご返信まで今しばらくお待ち下さいませ。

万が一、1週間以上たっても担当より返信がない場合は
回答が迷惑メールフォルダに入っているか、ご登録の
メールアドレスが間違っている可能性が御座いますので
ご確認をお願い致します。

それでも返答が見つからない場合、お手数をお掛け致しますが、
再度お問い合わせ下さいますようお願い申し上げます。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

【このメールにお心当たりのない方】
お心当たりのない場合は、このまま削除いただきますようにお願いいたします。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

Joinしよう！運営事務局（KARASTA.内）　 https://join-shimonoseki.com/
TEL 083-227-4747　営業時間 10:00～19:00（金土〜20:00／水曜休）
MAIL join@tmr-inc.jp

運営：
株式会社ザメディアジョン・リージョナル
〒750-0007 下関市赤間町1-10 創業支援カフェ KARASTA.内
TEL 083-227-4747
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
_message;

    $fromName = mb_encode_mimeheader('Joinしよう！運営事務局');
    $header = "From:{$fromName} <no-reply@join-shimonoseki.com>" .PHP_EOL;
    $header .= "Bcc:{$header_bcc}";

    mb_send_mail($mailto, $subject, $message, $header);

    $header = "From:{$fromName} <{$mailto}>" .PHP_EOL;
    $mailto_guest = $_POST['email'];

    mb_send_mail($mailto_guest,$subject,$message,$header);
}