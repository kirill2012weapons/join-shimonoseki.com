<aside class="sidebar">
    <h3>RECENT</h3>
    <ul class="list-sidebar">

        <?php

        if (wp_is_mobile()) {
            $query = new WP_Query(['posts_per_page' => '5']);
        } else {
            $query = new WP_Query(['posts_per_page' => '8']);
        }

        ?>

        <?php if ($query->have_posts()): while($query->have_posts()): $query->the_post(); ?>
            <li><a href="<?php the_permalink(); ?>"><strong><?= get_the_date('Y年n月j日'); ?></strong><?php the_title(); ?></a></li>
        <?php endwhile; endif; wp_reset_postdata(); ?>
    </ul>
    <h3>CATEGORY</h3>
    <ul class="list-sidebar">
        <li><a href="/news">すべて</a></li>
        <?php

        $categories = get_categories();

        ?>
        <?php foreach($categories as $category): ?>

            <?php

            $catQUERY = "
SELECT p.ID 
FROM wp_posts AS p 
INNER JOIN wp_term_relationships AS tr 
ON p.ID = tr.object_id 
INNER JOIN wp_term_taxonomy tt 
ON tr.term_taxonomy_id = tt.term_taxonomy_id 
AND p.post_type = 'post' 
AND tt.taxonomy = 'category' 
AND tt.term_id = " . $category->term_taxonomy_id . " 
AND p.post_status = 'publish' 
ORDER BY p.post_date DESC 
LIMIT 1";

            global $wpdb;

            $catQUERY_results = $wpdb->get_results( $catQUERY, OBJECT );

            ?>

            <?php if (!empty($catQUERY_results)) : ?>
                <li><a href="<?= add_query_arg('join', $catQUERY_results[0]->ID, get_category_link($category->cat_ID)); ?>"><?= $category->name; ?></a></li>
            <?php else : ?>
                <li><a href="<?= get_category_link($category->cat_ID); ?>"><?= $category->name; ?></a></li>
            <?php endif; ?>

        <?php endforeach; ?>
    </ul>
    <h3>ARCHIVE</h3>
    <ul class="list-sidebar">
        <li>
            <a href="<?= add_query_arg('y', 2018, 'https://join-shimonoseki.com/news') ?>">2018年</a>
        </li>
        <li>
            <a href="<?= add_query_arg('y', 2019, 'https://join-shimonoseki.com/news') ?>">2019年</a>
        </li>
    </ul>
</aside>