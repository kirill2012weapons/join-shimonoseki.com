<?php get_header(); ?>
    <section class="top-block appear op anim-delay-2">
        <div class="info">
            <h1>
					<span>
						<mark>下関</mark>で<br/>就活するなら
						<strong class="info-logo">Join</strong>
					</span>
            </h1>
        </div>
        <div class="scroller">
            <div><img src="<?= ASSETS ?>images/img-top-2.jpg" alt="image 1"></div>
        </div>
    </section>

    <section class="news-block">
        <h2 class="title-news">news</h2>
        <ul class="list-news">
            <?php $query = new WP_Query(['posts_per_page' => 3]); ?>
            <?php if ($query->have_posts()): while ($query->have_posts()): $query->the_post(); ?>
                <li>
                    <a href="<?php the_permalink(); ?>">
                        <span class="data"><?= get_the_date('Y.n.j'); ?></span>
                        <span><?php the_title(); ?></span>
                        <i>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.6 6.6" class="c-arrow">
                                <path data-name="arrow" d="M26.3,0.4l7.8,5.7H0" fill="none"></path>
                            </svg>
                        </i>
                    </a>
                </li>
            <?php endwhile; endif;
            wp_reset_postdata(); ?>
        </ul>
        <a href="/news" class="btn-more">
            more
            <i>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.6 6.6" class="c-arrow">
                    <path data-name="arrow" d="M26.3,0.4l7.8,5.7H0" fill="none"></path>
                </svg>
            </i>
        </a>
    </section>

    <section id="anhor01" class="about-project-block">
        <div class="holder">
            <h2 class="title-block">ABOUT PROJECT</h2>
            <h3 class="about-title-sub">
                下関市 若者の地元就職支援事業
                <mark>まちなかのキャリアセンタープロジェクト</mark>
            </h3>
            <strong class="logo">Join しよう!</strong>
            <div class="two-col">
                <div class="col"><img src="<?= ASSETS ?>images/img-about-project-block.png" alt="まちなかのキャリアセンター"/></div>
                <div class="col">
                    <h4>マッチングを実現 <br/>
                        学生と企業を Join させる</h4>
                    <p>「Join（ジョイン）する」。今の学生・若者たちは、
                        何かの取組（仕事）やその枠組みに入る際に、そう形容。
                        そう、下関に「Joinしよう！」。それを、
                        「まちなかのキャリアセンター（KARASTA.）」がサポート</p>
                </div>
            </div>
        </div>
    </section>

    <section id="anhor02" class="event-block">
        <div class="holder">
            <h2>EVENT</h2>
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide item01">
                        <div class="wrap">
                            <h3>2019.
                                <mark>2.13</mark>
                                (WED)
                            </h3>
                            <h4>しものせき企業博</h4>
                            <p>@海峡メッセ　イベントホール</p>
                            <div class="fram">
                                <dl>
                                    <dt>対象：</dt>
                                    <dd>大学生等の採用計画があり、下関市内に就業場所のある企業</dd>
                                </dl>
                                <dl>
                                    <dt>目的：</dt>
                                    <dd>若者の採用及び育成に積極的な企業と若者の出会いの場</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide item02">
                        <div class="wrap">
                            <h3>2018.
                                <mark>12.14</mark>
                                (FRI) <br/><span>18:30-21:30</span></h3>
                            <strong class="sub-titl">学生目線で企業の採用キャッチフレーズを考える！</strong>
                            <h4>企業と学生のしゃべり場</h4>
                            <p>@旧英国領事館　<span style="display: inline-block !important;" class="pc">下関市唐戸町4-11</span> <span class="m">先着20名／参加費無料</span></p>
                            <div class="fram">
                                <dl>
                                    <dt>対象：</dt>
                                    <dd>2020年以降に卒業予定の大学生 <span class="pc">（大学3年生以下）、短大生、専門学校生、専修学校生</span> <span class="sp">、短大・専門・専修学校生</span>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>参加企業：</dt>
                                    <dd>サンデングループ、住吉グループ、林兼グループほか、銀行系グループなど</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide item03">
                        <div class="wrap">
                            <h3>2018.
                                <mark>7.27</mark>
                                (FRI) <span>14:00-16:00</span></h3>
                            <strong class="sub-titl">失敗しない採用広報の手法</strong>
                            <h4>第 2 回採用力向上セミナー</h4>
                            <p>@下関市役所　新館５階会議室 <span class="m"><span>石渡嶺司氏</span>（大学ジャーナリスト・作家）</span></p>
                            <div class="fram">
                                <dl>
                                    <dt>対象：</dt>
                                    <dd>大学生等の採用計画があり、下関市内に就業場所のある企業</dd>
                                </dl>
                                <dl>
                                    <dt>目的：</dt>
                                    <dd>企業の経営者、人事担当者を対象に、近年の採用市場の現状や企業の取組みについて学ぶ</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"><span>NEXT<i><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.6 6.6"
                                                                  class="c-arrow"><path data-name="arrow"
                                                                                        d="M26.3,0.4l7.8,5.7H0"
                                                                                        fill="none"></path></svg></i></span>
                </div>
                <div class="swiper-button-prev"><span>BACK<i><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.6 6.6"
                                                                  class="c-arrow"><path data-name="arrow"
                                                                                        d="M26.3,0.4l7.8,5.7H0"
                                                                                        fill="none"></path></svg></i></span>
                </div>
            </div>
            <svg class="svg01" version="1.2" width="100%" height="100px" xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="0 0 100 100"
                 preserveAspectRatio="none">
					<polygon fill="#36acff" points="0,100 0,0 100,100"/>
				</svg>
        </div>
        <svg class="svg02" version="1.2" width="100%" height="100px" xmlns="http://www.w3.org/2000/svg"
             xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="0 0 100 100" preserveAspectRatio="none">
				<polygon fill="#fff" points="0,100 100,0 100,100"/>
			</svg>
        <div class="scroller scroller_top">
            <div><img src="<?= ASSETS ?>images/img-top-2.jpg" alt="image 1"></div>
        </div>
        <div class="scroller scroller_bot">
            <div><img src="<?= ASSETS ?>images/img-top-2.jpg" alt="image 1"></div>
        </div>
    </section>


    <section id="anhor03" class="contents-block">
        <h2 class="title-block">CONTENTS</h2>
        <ul class="contents-list">
            <li class="items1 appear tr_b anim-delay-1">
                <span class="n">1.</span>
                <div class="image">
                    <img class="sp" src="<?= ASSETS ?>images/img005.png" alt="Job Fair"/>
                    <img class="pc" src="<?= ASSETS ?>images/img1.jpg" alt="Job Fair"/>
                </div>
                <strong class="i"><span>Job Fair</span></strong>
                <div class="text-info">
                    <h3>
                        運命の学生・企業との出会いの場
                        <mark>しものせき企業博</mark>
                    </h3>
                    <p>
								<b>ご来場ありがとうございました！</b>
                        2019年2月13日(水) <br>
                        ＠海峡メッセ　イベントホール <br>
								下関市史上最大級！就活スタートアップイベント<br>
								第1部 個別面接会 11:00～12:00<br>
								第2部 業界研究会 13:00～16:00<br>
							</p>
                    <strong>
								<a href="https://join-shimonoseki.com/wp-content/uploads/2019/01/gyokaikenkyukai.pdf" target=”_blank”>参加企業はこちらから→</a><br>
								<a href="https://join-shimonoseki.com/wp-content/uploads/2018/12/190213_join_pos_181220_2000.png" target=”_blank”>ポスターダウンロード→</a><br>
								開催実績はこちらから</strong>
                    <a href="https://join-shimonoseki.com/events/190213_kigyohaku_report/" class="btn-join">News</a>
                </div>
                <img class="sp iimage" src="<?= ASSETS ?>images/img1i.png" alt="Matching"/>
            </li>
            <li class="items2 appear tr_b anim-delay-1">
                <span class="n">2.</span>
                <div class="image">
                    <img class="sp" src="<?= ASSETS ?>images/img006.png" alt="Discussion"/>
                    <img class="pc" src="<?= ASSETS ?>images/img2.jpg" alt="Discussion"/>
                </div>
                <strong class="i"><span>Discussion</span></strong>
                <div class="text-info">
                    <h3>
                        学生目線で企業の採用キャッチフレーズを考える！
                        <mark>企業と学生のしゃべり場</mark>
                    </h3>
                    <p>
							<span class="add01">
								2018年12月14日（金） <br>
								18:30〜21:30（受付 18:00〜）<br>
								@旧英国領事館
							</span>
                        <span class="add">
								対象：2020年以降に卒業予定の大学生（3年生以下）、短大生、専門学校生、専修学校生＜先着20名＞<br>
							参加企業：サンデングループ（交通）、住吉グループ（建設・土木）、林兼グループ（食品ほか）ほか、銀行グループなど<br>
							軽食・飲み物あり／服装は私服OK<br>
							※公共交通機関をご利用ください。<br>
							※参加には事前予約が必要です。
							</span>
                    </p>
                    <strong>開催実績はこちらから</strong>
                    <a href="https://join-shimonoseki.com/events/%E4%BC%81%E6%A5%AD%E3%81%A8%E5%AD%A6%E7%94%9F%E3%81%AE%E3%81%97%E3%82%83%E3%81%B9%E3%82%8A%E5%A0%B4%EF%BC%88%E7%AC%AC%EF%BC%91%E5%9B%9E%E4%BC%81%E6%A5%AD%E3%81%A8%E5%AD%A6%E7%94%9F%E3%81%AE%E4%BA%A4/" class="btn-join">News</a>
                </div>
                <img class="sp iimage" src="<?= ASSETS ?>images/img2i.png" alt="Matching"/>
            </li>
            <li class="items3 appear tr_b anim-delay-1">
                <span class="n">3.</span>
                <div class="image">
                    <img class="sp" src="<?= ASSETS ?>images/img001.png" alt="Matching"/>
                    <img class="pc" src="<?= ASSETS ?>images/img3.jpg" alt="Matching"/>
                </div>
                <strong class="i"><span>Matching</span></strong>
                <div class="text-info">
                    <h3>
                        運命の学生・企業との出会いの場
                        <mark>就職面接会 <br/>in しものせき</mark>
                    </h3>
                    <p>
                        <b>ご来場ありがとうございました！</b>
                        <span class="add">
								2019年卒採用向け「大学生等就職面接会inしものせき」<br/>
								2018年8月17日（土）海峡メッセにて開催しました。
							</span>
                    </p>
                    <strong>開催の様子はこちらから</strong>
                    <a href="/join" class="btn-join">News</a>
                </div>
                <img class="sp iimage" src="<?= ASSETS ?>images/img3i.png" alt="Matching"/>
            </li>
            <li class="items4 appear tr_b anim-delay-1">
                <span class="n">4.</span>
                <div class="image">
                    <img class="sp" src="<?= ASSETS ?>images/img002.png" alt="Movies"/>
                    <img class="pc" src="<?= ASSETS ?>images/img4.jpg" alt="Movies"/>
                </div>
                <strong class="i"><span>Movies</span></strong>
                <div class="text-info">
                    <h3>
                        企業の魅力を学生目線で配信
                        <mark>しものせきレポート</mark>
                    </h3>
                    <p>市外の学生を中心に企業のPR動画を作成！完成後は、YoutubeやHPに掲載します。<br/>企業への取材、動画の作成も全て学生主体です。イマドキ学生の感性を集め、学生が「働いてみたいな」と思えるようなPR動画を作成します。取材にご協力いただける企業はぜひお問い合わせください。
                    </p>
                    <strong>取材を行いたい学生は以下よりご応募</strong>
                    <a href="/join" class="btn-join">Join</a>
                </div>
                <img class="sp iimage" src="<?= ASSETS ?>images/img4i.png" alt="Matching"/>
            </li>
            <li class="items5 appear tr_b anim-delay-1">
                <span class="n">5.</span>
                <div class="image">
                    <img class="sp" src="<?= ASSETS ?>images/img003.png" alt="Seminar"/>
                    <img class="pc" src="<?= ASSETS ?>images/img5.jpg" alt="Seminar"/>
                </div>
                <strong class="i"><span>Seminar</span></strong>
                <div class="text-info">
                    <h3>
                        イマドキ学生・就活最前線を理解しよう
                        <mark>採用力向上セミナー</mark>
                    </h3>
                    <p>「新卒採用を検討している」「採用がうまくいかない」という企業様向けのセミナーです。<br/>
                        県内外から講師を招き、自社の採用を見直し、より良い採用方法を見つけていただける場を提供します。（参加費無料）<br/>
                        ＜2019年卒採用向けセミナースケジュール＞ <br/>
                        2018年7月10日・27日 and more</p>
                    <strong>参加希望の企業は以下よりエントリー</strong>
                    <a href="/join" class="btn-join">Join</a>
                </div>
                <img class="sp iimage" src="<?= ASSETS ?>images/img5i.png" alt="Matching"/>
            </li>
            <li class="items6 appear tr_b anim-delay-1">
                <span class="n">6.</span>
                <div class="image">
                    <img class="sp" src="<?= ASSETS ?>images/img004.png" alt="Relationship"/>
                    <img class="pc" src="<?= ASSETS ?>images/img6.jpg" alt="Relationship"/>
                </div>
                <strong class="i"><span>Relationship</span></strong>
                <div class="text-info">
                    <h3>
                        企業と学生―お互いに理解しよう
                        <mark>下関の学生と企業の
                            交流
                        </mark>
                    </h3>
                    <p>
                        学生と企業が繋がる交流会を開催します。就活に悩む学生の皆さん！実際に働く方々と軽食を交えて交流しませんか。交流会をとおして、企業の人事担当者と「就活トーク」しませんか。企業の方も、実際の学生と交流できるチャンス！
                        <br/>
                        （企業・学生：参加無料）</p>
                    <strong>参加希望の企業は以下よりエントリー</strong>
                    <a href="/join" class="btn-join">Join</a>
                </div>
                <img class="sp iimage" src="<?= ASSETS ?>images/img06i.png" alt="Matching"/>
            </li>
        </ul>
    </section>

    <section class="product-block">
        <svg class="pc" version="1.1" width="100%" height="900px" xmlns="http://www.w3.org/2000/svg"
             xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="0 0 100 100"
             preserveAspectRatio="none">
				<polygon fill="#1e1e1e" points="0 20, 0 85, 100 100, 100 10"></polygon>
			</svg>
        <svg class="sp" version="1.1" width="100%" height="1000px" xmlns="http://www.w3.org/2000/svg"
             xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="0 0 100 100"
             preserveAspectRatio="none">
				<polygon fill="#1e1e1e" points="0 5, 0 95, 100 100, 100 0"></polygon>
			</svg>
        <div class="scroller">
            <div><img src="<?= ASSETS ?>images/img-top-2.jpg" alt="image 1"></div>
        </div>
        <div id="anhor04" class="holder">
            <div class="colums">
                <div class="col">
                    <div class="frame">
                        <h2 class="title-block">PRODUCT</h2>
                        <span class="info">2019年3月リリース</span>
                        <h3>下関就活アプリ
                            <mark>Job net</mark>
                        </h3>
                        <p>たくさんのニーズにお応えして、下関市の地元就職応援アプリを開発・運営しています。学生や保護者の「知りたい」に対応した、情報配信を行っていきます。<br/>
										<a href="https://itunes.apple.com/jp/app/%E3%81%97%E3%82%82%E3%81%AE%E3%81%9B%E3%81%8D-job-net/id1450134350?mt=8" target=”_blank” style="color:#ffffff">iOSアプリダウンロードはこちらから→</a><br>
										<a href="https://play.google.com/store/apps/details?id=jp.lg.shimonoseki.city.smartphone.app.jobnet" target=”_blank” style="color:#ffffff">Androidアプリダウンロードはこちらから→</a>
                        </p>
                    </div>
                </div>
                <div class="col">
                    <div class="image"><img src="<?= ASSETS ?>images/ico-phone.png" alt="PRODUCT"/></div>
                </div>
            </div>
        </div>
    </section>


    <section class="data-section">
        <div class="parallax">
            <img src="<?= ASSETS ?>images/icon-data.png" alt="2020"/>
        </div>
    </section>
    <section id="anhor05" class="next-project">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%"
             baseProfile="full" version="1.2" preserveAspectRatio="none" style="width: 100%;display: block;">
            <defs>
                <clipPath id="mask">
                    <path d=""></path>
                </clipPath>
                <linearGradient id="svg_grad" x1="0%" y1="0%" x2="100%" y2="0%">
                    <stop offset="0%" style="stop-color:rgb(3,177,239);stop-opacity:1"></stop>
                    <stop offset="100%" style="stop-color:rgb(20, 90, 219);stop-opacity:1"></stop>
                </linearGradient>
            </defs>
            <rect clip-path="url(#mask)" width="100%" height="100%" x="0" y="0" fill="url(#svg_grad)"></rect>
            <g clip-path="url(#mask)">
                <image class="pc" width="70%" height="100%" y="70" x="30%"
                       xlink:href="<?= ASSETS ?>images/image_skew.png"
                       preserveAspectRatio="xMidYMid slice"
                       style="opacity: .3;"></image>
                <image class="sp" width="70%" height="100%" y="30" x="30%"
                       xlink:href="<?= ASSETS ?>images/image_skew.png"
                       preserveAspectRatio="xMidYMid slice"
                       style="opacity: .3;"></image>
            </g>
        </svg>
        <div class="holder">
            <h2 class="title-news">NEXT PROJECT</h2>
            <p>合説・面接会！見据えるのは<br class="sp"/>2020年卒 就活・採用</p>
            <h3>就職フェア</h3>
            <p>参加費は無料です。<br/>
                2019年卒予定の大学４年生を対象とした「面接会」<br/>
                2020年卒予定の大学３年生を対象とした「説明会」<br/>
                を実施いたします。<br/>
                当日はイベントも開催します。<br/>
                2019年2月13日（水） ＠海峡メッセ</p>
            <strong>詳細は随時更新予定</strong>
        </div>
    </section>

    <section class="facility-block">
        <div id="anhor06" class="holder">
            <h2 class="title-block">FACILITY</h2>
            <h3>
                学生の就活相談相談所
                <mark>まちなかのキャリアセンターin KARASTA.</mark>
            </h3>
            <div class="colums">
                <div class="col">
                    <img src="<?= ASSETS ?>images/img_011.jpg" alt="FACILITY" />
                </div>
                <div class="col">
                    <div class="frame">
                        <p>唐戸商店街の中にある創業支援カフェ「KARASTA.」
                            にて、学生対象の就活セミナーやキャリアカウンセリングを受け付けます。<br />
                            利用料無料です。</p>
                        <a href="https://www.karasta.jp/" class="btn-join">KARASTA. WEBサイト</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="join-block">
        <svg class="pc" version="1.1"  width="100%" height="130px" xmlns="http://www.w3.org/2000/svg"
             xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="0 0 100 100"
             preserveAspectRatio="none" >
				<polygon fill="#fff" points="0 0, 0 100, 100 100"></polygon>
			</svg>
        <svg class="sp" version="1.1"  width="100%" height="40px" xmlns="http://www.w3.org/2000/svg"
             xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="0 0 100 100"
             preserveAspectRatio="none" >
				<polygon fill="#fff" points="0 0, 0 100, 100 100"></polygon>
			</svg>
        <div class="holder">
            <h2 class="title-block">JOIN</h2>
            <h3>下関で就活するなら「Joinしよう！」</h3>
            <p>各種お申し込みやご質問・ご相談など、まずはお気軽にお問い合わせください。</p>
            <strong class="tel">TEL. <a href="#">083-227-4747</a></strong>
            <a href="/join" class="btn-join">お問い合わせする</a>
            <ul class="soc-list">
                <li><a href="https://www.facebook.com/karasta.jp/" class="f">facebook</a></li>
                <li><a href="https://www.instagram.com/karasta.jp/" class="i">instagram</a></li>
                <li><a href="https://twitter.com/karastajp/" class="t">twitter</a></li>
            </ul>
        </div>
    </section>

<?php get_footer(); ?>