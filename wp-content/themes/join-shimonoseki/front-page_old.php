<?php get_header(); ?>
    <section class="top-block">
        <div class="info">
            <h1>
            <span>
                <mark>下関</mark>で<br/>就活するなら
                <strong class="info-logo">Join</strong>
            </span>
            </h1>
        </div>
        <div class="scroller">
            <div><img src="<?= ASSETS ?>images/img-top-2.jpg" alt="image 1"></div>
        </div>
    </section>
    <section class="news-block">
        <h2 class="title-news">news</h2>
        <ul class="list-news">
            <?php $query = new WP_Query(['posts_per_page' => 3]); ?>
            <?php if ($query->have_posts()): while ($query->have_posts()): $query->the_post(); ?>
                <li>
                    <a href="<?php the_permalink(); ?>">
                        <span class="data"><?= get_the_date('Y.n.j'); ?></span>
                        <span><?php the_title(); ?></span>
                        <i>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.6 6.6" class="c-arrow">
                                <path data-name="arrow" d="M26.3,0.4l7.8,5.7H0" fill="none"></path>
                            </svg>
                        </i>
                    </a>
                </li>
            <?php endwhile; endif; wp_reset_postdata(); ?>
        </ul>
        <a href="/news" class="btn-more">
            more
            <i>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.6 6.6" class="c-arrow">
                    <path data-name="arrow" d="M26.3,0.4l7.8,5.7H0" fill="none"></path>
                </svg>
            </i>
        </a>
    </section>
    <section id="anhor01" class="about-project-block">
        <div class="holder">
            <h2 class="title-block">ABOUT PROJECT</h2>
            <h3 class="about-title-sub">
                下関市 若者の地元就職支援事業
                <mark>まちなかのキャリアセンタープロジェクト</mark>
            </h3>
            <strong class="logo">Join しよう!</strong>
            <div class="two-col">
                <div class="col"><img src="<?= ASSETS ?>images/img-about-project-block.png" alt="まちなかのキャリアセンター"/></div>
                <div class="col">
                    <h4>マッチングを実現 <br/>
                        学生と企業を Join させる</h4>
                    <p>「Join（ジョイン）する」。今の学生・若者たちは、
                        何かの取組（仕事）やその枠組みに入る際に、そう形容。
                        そう、下関に「Joinしよう！」。それを、
                        「まちなかのキャリアセンター（KARASTA.）」がサポート</p>
                </div>
            </div>
        </div>
    </section>
    <section id="anhor02" class="event-block">
        <div class="holder">
            <h2>EVENT</h2>
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <a href="#anhor03" class="swiper-slide item01">
                        <div class="wrap">
                            <h3>2018.<mark>8.17</mark>(FRI) <span>13:30-16:30</span></h3>
                            <h4>大学生等就職面接会 <br/>inしものせき</h4>
                            <p>@海峡メッセ下関　イベントホール</p>
                            <div class="fram">
                                <dl>
                                    <dt>対象：</dt>
                                    <dd>若者の採用及び育成に積極的な企業と若者の出会いの場</dd>
                                </dl>
                                <dl>
                                    <dt>目的：</dt>
                                    <dd>大学生等の採用計画があり、下関市内に就業場所のある企業</dd>
                                </dl>
                            </div>
                        </div>
                    </a>
                    <a href="#anhor-slide-3" class="swiper-slide item02">
                        <div class="wrap">
                            <h3>2018.<mark>7.10</mark>(TUE)<span>14:00-16:00</span></h3>
                            <strong class="sub-titl">エントリー「０」から始める、新しい新卒採用のやり方―最新の成功事例を交えて</strong>
                            <h4>第 1 回企業の採用力向上セミナー</h4>
                            <p>@下関市役所　新館５階会議室 <span class="m"><span>杉浦二郎氏</span> （㈱モザイクワーク 代表取締役・採用学研究所フェロー）</span></p>
                            <div class="fram">
                                <dl>
                                    <dt>対象：</dt>
                                    <dd>若者の採用及び育成に積極的な企業と若者の出会いの場</dd>
                                </dl>
                                <dl>
                                    <dt>目的：</dt>
                                    <dd>大学生等の採用計画があり、下関市内に就業場所のある企業</dd>
                                </dl>
                            </div>
                        </div>
                    </a>
                    <a href="#anhor-slide-3" class="swiper-slide item03">
                        <div class="wrap">
                            <h3>2018.
                                <mark>7.27</mark>
                                (FRI) <span>14:00-16:00</span></h3>
                            <strong class="sub-titl">失敗しない採用広報の手法</strong>
                            <h4>第 2 回企業の採用力向上セミナー</h4>
                            <p>@下関市役所　新館５階会議室 <span class="m"><span>石渡嶺司氏</span>（大学ジャーナリスト・作家）</span></p>
                            <div class="fram">
                                <dl>
                                    <dt>対象：</dt>
                                    <dd>若者の採用及び育成に積極的な企業と若者の出会いの場</dd>
                                </dl>
                                <dl>
                                    <dt>目的：</dt>
                                    <dd>大学生等の採用計画があり、下関市内に就業場所のある企業</dd>
                                </dl>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"><span>NEXT<i><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.6 6.6"
                                                                  class="c-arrow"><path data-name="arrow"
                                                                                        d="M26.3,0.4l7.8,5.7H0"
                                                                                        fill="none"></path></svg></i></span>
                </div>
                <div class="swiper-button-prev"><span>BACK<i><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.6 6.6"
                                                                  class="c-arrow"><path data-name="arrow"
                                                                                        d="M26.3,0.4l7.8,5.7H0"
                                                                                        fill="none"></path></svg></i></span>
                </div>
            </div>
            <svg class="svg01" version="1.2" width="100%" height="100px" xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="0 0 100 100"
                 preserveAspectRatio="none">
					<polygon fill="#36acff" points="0,100 0,0 100,100"/>
				</svg>
        </div>
        <svg class="svg02" version="1.2" width="100%" height="100px" xmlns="http://www.w3.org/2000/svg"
             xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="0 0 100 100"
             preserveAspectRatio="none">
				<polygon fill="#fff" points="0,100 100,0 100,100"/>
			</svg>
        <div class="scroller scroller_top">
            <div><img src="<?= ASSETS ?>images/img-top-2.jpg" alt="image 1"></div>
        </div>
        <div class="scroller scroller_bot">
            <div><img src="<?= ASSETS ?>images/img-top-2.jpg" alt="image 1"></div>
        </div>
    </section>
    <section id="anhor03" class="contents-block">
        <h2 class="title-block">CONTENTS</h2>
        <ul class="contents-list">
            <li class="appear tr_b anim-delay-1">
                <span class="n">1.</span>
                <div class="image">
                    <img class="sp" src="<?= ASSETS ?>images/img001.png" alt="Matching"/>
                    <img class="pc" src="<?= ASSETS ?>images/img3.jpg" alt="Matching"/>
                </div>
                <strong class="i"><span>Matching</span></strong>
                <div class="text-info">
                    <h3>運命の学生・企業との出会いの場
                        <mark>大学生等就職面接会 in しものせき</mark>
                    </h3>
                    <p>
                        2019年卒採用向け「大学生等就職面接会 in しものせき」 <br>
                        2018年8月17日（金）@海峡メッセ下関＜参加費無料＞ <br>
                        下関の新卒採用に熱い企業が集結！<br>
                        当日参加もOKです。留学生もぜひご来場ください。<br/>
                        就活に悩む方用の相談コーナーや留学生の相談コーナーも設置します。<br/>
                        チラシ→７月上旬アップ予定</p>
                    <strong>参加希望の学生は以下よりエントリー</strong>
                    <a href="/join" class="btn-join" style="margin-bottom: 15px;">Join</a>
                    <a href="/2018/07/17/kigyolist180817/" class="btn-join" id="btn-sanca">参加企業一覧</a>
                </div>
                <img class="sp iimage" src="<?= ASSETS ?>images/img3i.png" alt="Matching"/>
            </li>
            <li class="appear tr_b anim-delay-1">
                <span class="n">2.</span>
                <div class="image">
                    <img class="sp" src="<?= ASSETS ?>images/img002.png" alt="Movies"/>
                    <img class="pc" src="<?= ASSETS ?>images/img4.jpg" alt="Movies"/>
                </div>
                <strong class="i"><span>Movies</span></strong>
                <div class="text-info">
                    <h3>
                        企業の魅力を学生目線で配信
                        <mark>しものせき企業レポート</mark>
                    </h3>
                    <p>市外の学生を中心に企業のPR動画を作成！完成後は、YoutubeやHPに掲載します。<br/>企業への取材、動画の作成も全て学生主体です。イマドキ学生の感性を集め、学生が「働いてみたいな」と思えるようなPR動画を作成します。取材にご協力いただける企業はぜひお問い合わせください。
                    </p>
                    <strong>取材を行いたい学生は以下よりご応募</strong>
                    <a href="/join" class="btn-join">Join</a>
                </div>
                <img class="sp iimage" src="<?= ASSETS ?>images/img4i.png" alt="Matching"/>
            </li>
            <li class="appear tr_b anim-delay-1" id="anhor-slide-3">
                <span class="n">3.</span>
                <div class="image">
                    <img class="sp" src="<?= ASSETS ?>images/img003.png" alt="Seminar"/>
                    <img class="pc" src="<?= ASSETS ?>images/img5.jpg" alt="Seminar"/>
                </div>
                <strong class="i"><span>Seminar</span></strong>
                <div class="text-info">
                    <h3>
                        イマドキ学生・就活最前線を理解しよう
                        <mark>企業の採用力向上セミナー</mark>
                    </h3>
                    <p>「新卒採用を検討している」「採用がうまくいかない」という企業様向けのセミナーです。<br/>
                        県内外から講師を招き、自社の採用を見直し、より良い採用方法を見つけていただける場を提供します。（参加費無料）<br/>
                        ＜2019年卒採用向けセミナースケジュール＞ <br/>
                        2018年7月10日・27日 and more</p>
                    <strong>参加希望の企業は以下よりエントリー</strong>
                    <a href="/join" class="btn-join">Join</a>
                </div>
                <img class="sp iimage" src="<?= ASSETS ?>images/img5i.png" alt="Matching"/>
            </li>
            <li class="appear tr_b anim-delay-1">
                <span class="n">4.</span>
                <div class="image">
                    <img class="sp" src="<?= ASSETS ?>images/img004.png" alt="Relationship"/>
                    <img class="pc" src="<?= ASSETS ?>images/img6.jpg" alt="Relationship"/>
                </div>
                <strong class="i"><span>Relationship</span></strong>
                <div class="text-info">
                    <h3>
                        企業と学生―お互いに理解しよう
                        <mark>
                            企業と学生の交流会
                        </mark>
                    </h3>
                    <p>
                        学生と企業が繋がる交流会を開催します。就活に悩む学生の皆さん！実際に働く方々と軽食を交えて交流しませんか。交流会をとおして、企業の人事担当者と「就活トーク」しませんか。企業の方も、実際の学生と交流できるチャンス！
                        <br/>
                        （企業・学生：参加無料）</p>
                    <strong>参加希望の企業は以下よりエントリー</strong>
                    <a href="/join" class="btn-join">Join</a>
                </div>
                <img class="sp iimage" src="<?= ASSETS ?>images/img06i.png" alt="Matching"/>
            </li>
        </ul>
    </section>
    <section class="product-block">
        <svg class="pc" version="1.1" width="100%" height="900px" xmlns="http://www.w3.org/2000/svg"
             xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="0 0 100 100"
             preserveAspectRatio="none">
				<polygon fill="#1e1e1e" points="0 20, 0 85, 100 100, 100 10"></polygon>
			</svg>
			<svg class="sp" version="1.1" width="100%" height="1000px" xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="0 0 100 100"
                 preserveAspectRatio="none">
				<polygon fill="#1e1e1e" points="0 5, 0 95, 100 100, 100 0"></polygon>
			</svg>
        <div class="moving-image"></div>
        <div class="scroller">
            <div><img src="<?= ASSETS ?>images/img-top-2.jpg" alt="image 1"></div>
        </div>
        <div id="anhor04" class="holder">
            <div class="colums">
                <div class="col">
                    <div class="frame">
                        <h2 class="title-block">PRODUCT</h2>
                        <span class="info">2018年冬リリース</span>
                        <h3>下関就活アプリ
                            <mark>Job net</mark>
                        </h3>
                        <p>たくさんのニーズにお応えして、下関市の地元就職応援アプリを開発・運営します。リリースは2018年冬を予定しています。学生や保護者の「知りたい」に対応した、情報配信を行っていきます。<br/>※最新情報は後日掲載します
                        </p>
                    </div>
                </div>
                <div class="col">
                    <div class="image"><img src="<?= ASSETS ?>images/ico-phone.png" alt="PRODUCT"/></div>
                </div>
            </div>
        </div>
    </section>
    <section class="data-section">
        <div class="parallax">
            <img src="<?= ASSETS ?>images/icon-data.png" alt="2020"/>
        </div>
    </section>
    <section id="anhor05" class="next-project">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%"
             baseProfile="full" version="1.2" preserveAspectRatio="none" style="width: 100%;display: block;">
            <defs>
                <clipPath id="mask">
                    <path d=""></path>
                </clipPath>
                <linearGradient id="svg_grad" x1="0%" y1="0%" x2="100%" y2="0%">
                    <stop offset="0%" style="stop-color:rgb(3,177,239);stop-opacity:1"></stop>
                    <stop offset="100%" style="stop-color:rgb(20, 90, 219);stop-opacity:1"></stop>
                </linearGradient>
            </defs>
            <rect clip-path="url(#mask)" width="100%" height="100%" x="0" y="0" fill="url(#svg_grad)"></rect>
            <g clip-path="url(#mask)">
                <image class="pc" width="70%" height="100%" y="70" x="30%"
                       xlink:href="<?= ASSETS ?>images/image_skew.png"
                       preserveAspectRatio="xMidYMid slice"
                       style="opacity: .3;"></image>
                <image class="sp" width="70%" height="100%" y="30" x="30%"
                       xlink:href="<?= ASSETS ?>images/image_skew.png"
                       preserveAspectRatio="xMidYMid slice"
                       style="opacity: .3;"></image>
            </g>
        </svg>
        <div class="holder">
            <h2 class="title-news">NEXT PROJECT</h2>
            <p>合説・面接会！見据えるのは<br class="sp"/>2020年卒 就活・採用</p>
            <h3>しものせき企業合同就職フェア</h3>
            <p>参加費は無料です。<br/>
                2019年卒予定の大学４年生を対象とした「面接会」<br/>
                2020年卒予定の大学３年生を対象とした「説明会」<br/>
                を実施します。<br/>
                当日はイベントも開催します。<br/>
                2019年2月13日（水） ＠海峡メッセ下関</p>
            <strong>詳細は随時更新予定</strong>
        </div>
    </section>
    <section class="facility-block">
        <div id="anhor06" class="holder">
            <h2 class="title-block">FACILITY</h2>
            <h3>
                学生の就活相談所
                <mark>まちなかのキャリアセンターin KARASTA.</mark>
            </h3>
            <div class="colums">
                <div class="col">
                    <img src="<?= ASSETS ?>images/img_011.jpg" alt="FACILITY"/>
                </div>
                <div class="col">
                    <div class="frame">
                        <p>唐戸商店街の中にある創業支援カフェ「KARASTA.」
                            にて、学生対象の就活セミナーやキャリアカウンセリングを受け付けます。<br/>
                            利用料無料です。</p>
                        <a href="https://www.karasta.jp/" class="btn-join" target="_blank">KARASTA. WEBサイト</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="join-block">
        <svg class="pc" version="1.1" width="100%" height="130px" xmlns="http://www.w3.org/2000/svg"
             xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="0 0 100 100"
             preserveAspectRatio="none">
				<polygon fill="#fff" points="0 0, 0 100, 100 100"></polygon>
			</svg>
			<svg class="sp" version="1.1" width="100%" height="40px" xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="0 0 100 100"
                 preserveAspectRatio="none">
				<polygon fill="#fff" points="0 0, 0 100, 100 100"></polygon>
			</svg>
        <div class="holder">
            <h2 class="title-block">JOIN</h2>
            <h3>下関で就活するなら「Joinしよう！」</h3>
            <p>各種お申し込みやご質問・ご相談など、まずはお気軽にお問い合わせください。</p>
            <strong class="tel">TEL. <a href="tel:0832274747">083-227-4747</a></strong>
            <a href="/join" class="btn-join">お問い合わせ</a>
            <ul class="soc-list">
                <li><a class="f" href="https://www.facebook.com/karasta.jp/" target="_blank">facebook</a></li>
                <li><a class="i" href="https://www.instagram.com/karasta.jp/" target="_blank">instagram</a></li>
                <li><a class="t" href="https://twitter.com/karastajp/" target="_blank">twitter</a></li>
            </ul>
        </div>
    </section>
<?php get_footer(); ?>