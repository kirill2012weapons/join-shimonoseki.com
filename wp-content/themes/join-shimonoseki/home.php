<?php get_header(); ?>

<?php if (!isset($_GET['y'])) : ?>
    <section class="top-section" data-home-php>
        <h2 class="title-block"><?= single_post_title(); ?></h2>
        <svg class="svg02" version="1.1" width="100%" height="100px" xmlns="http://www.w3.org/2000/svg"
             xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="0 0 100 100"
             preserveAspectRatio="none">
            <polygon fill="#fff" points="0,100 100,0 100,100"/>
        </svg>
        <div class="scroller">
            <div>
                <img src="<?= ASSETS ?>images/img-top-2.jpg" alt="image 1">
            </div>
        </div>
    </section>

    <?php

    $pageS = 1;

    if (isset($_GET['pn'])) {

        if ($_GET['pn'] <= 1) $pageS = 1;
        else $pageS = $_GET['pn'];

    }

    $queryNews = new WP_Query([
        'posts_per_page' => 30,
        'offset' => ($pageS - 1) * 30,
    ]);

    $countSQL = "
    SELECT COUNT( * ) AS count_posts_all
    FROM wp_posts
    WHERE post_type = 'post' 
    AND post_status NOT IN ( 'trash','auto-draft','inherit','request-pending','request-confirmed','request-failed','request-completed' )
    ";

    global $wpdb;

    $count = $wpdb
        ->get_results($countSQL)[0]
        ->count_posts_all;

    ?>

    <?php if ($queryNews->have_posts()): ?>
        <section class="news-list">
            <ul class="list-news">
                <?php $have = false; ?>
                <?php while ($queryNews->have_posts()): $queryNews->the_post(); ?>
                    <?php $have = true; ?>
                    <li>
                        <a href="<?= get_the_permalink(); ?>">
                            <span class="data"><?= get_the_date('Y.n.j'); ?></span>
                            <span><?php the_title(); ?></span>
                            <i>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.6 6.6" class="c-arrow">
                                    <defs>
                                        <linearGradient id="lgrad" x1="0%" y1="100%" x2="100%" y2="0%">
                                            <stop offset="0%" style="stop-color:rgb(2,180,239);stop-opacity:1"></stop>
                                            <stop offset="1%" style="stop-color:rgb(2,180,239);stop-opacity:1"></stop>
                                            <stop offset="100%" style="stop-color:rgb(22,80,216);stop-opacity:1"></stop>
                                        </linearGradient>
                                    </defs>
                                    <path data-name="arrow" d="M26.3,0.4l7.8,5.7H0" stroke="url(#lgrad)" fill="none"></path>
                                </svg>
                            </i>
                        </a>
                    </li>
                <?php endwhile; ?>
            </ul>
            <?php if ($have) : ?>
                <div class="news-section" style="margin-top: 75px;">
                    <div class="two-colums">
                        <div class="btn-next">

                            <?php if ($pageS > 1) : ?>
                                <a class="btn-next-prev" href="<?= add_query_arg('pn', $pageS-1, $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']) ?>">PREV</a>
                            <?php endif; ?>

                            <?php if (($count/30) > $pageS) : ?>
                                <a class="btn-next-next" href="<?= add_query_arg('pn', $pageS+1, $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']) ?>">NEXT</a>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </section>
    <?php endif; ?>

<?php else: ?>

    <?php

    if (!isset($_GET['pn'])) $pn = 1;
    else $pn = $_GET['pn'];

    $year = $_GET['y'];

    $queryArchive = new WP_Query([
        'post_type' => 'post',
        'post_status' => 'publish',
        'year' => $year,
        'posts_per_page' => 1,
        'offset' => $pn-1,
        'orderby' => 'date',
        'order' => 'DESC',
    ]);

    $queryNextArchive = new WP_Query([
        'post_type' => 'post',
        'post_status' => 'publish',
        'year' => $year,
        'posts_per_page' => 1,
        'offset' => $pn,
        'orderby' => 'date',
        'order' => 'DESC',
    ]);

    if ($pn > 1) $btnPrev = '<a class="btn-next-prev" href="' . add_query_arg(['y' => $year, 'pn' => $pn - 1], 'https://join-shimonoseki.com/news') . '">PREV</a>';
    else $btnPrev = '';

    if ($queryNextArchive->have_posts()) $btnNext = '<a class="btn-next-next" href="' . add_query_arg(['y' => $year, 'pn' => $pn + 1], 'https://join-shimonoseki.com/news') . '">NEXT</a>';
    else $btnNext = '';

    ?>

    <section class="top-section" data-archive-php>
        <h2 class="title-block">NEWS</h2>
        <svg class="svg02" version="1.1"  width="100%" height="100px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="0 0 100 100" preserveAspectRatio="none" >
        <polygon fill="#fff" points="0,100 100,0 100,100"/>
    </svg>
        <div class="scroller">
            <div><img src="<?= ASSETS ?>images/img-top-2.jpg" alt="image 1"></div>
        </div>
    </section>
    <section class="news-section">
        <div class="holder">
            <div class="two-colums">
                <section class="main">
                    <?php if ($queryArchive->have_posts()): while($queryArchive->have_posts()): $queryArchive->the_post(); ?>
                        <div class="news-info">
                            <?= get_the_date('Y年n月j日'); ?>
                            <?php $categories = get_the_category(); ?>
                            <?php foreach ($categories as $category): ?>
                                <a href="<?= get_category_link($category->cat_ID); ?>"><?= $category->name; ?></a>
                            <?php endforeach; ?>
                        </div>
                        <h2><?php the_title(); ?></h2>
                        <?php the_content(); ?>
                        <div class="btn-next">
                            <?php
//                            $previous_posts_link = get_previous_posts_link('PREV');
//                            $next_posts_link = get_next_posts_link('NEXT');
//                            if ($previous_posts_link) echo substr_replace($previous_posts_link, 'class="btn-next-prev"', 3, 0);

                            echo $btnPrev;

                            echo '<a class="btn-next-next btn-news" href="https://join-shimonoseki.com/news">NEWS 一覧へ</a>';

                            echo $btnNext;

//                            if ($next_posts_link) echo substr_replace($next_posts_link, 'class="btn-next-next"', 3, 0);
                            ?>
                        </div>
                    <?php endwhile; endif; wp_reset_postdata(); ?>
                </section>
                <?php get_sidebar(); ?>
            </div>
        </div>
    </section>

<?php endif; ?>

<?php get_footer(); ?>
