$(document).on('ready', function() {

    var _joinErrorValidation = new joinErrorValidation();

    mobileMenu();
    swiperGallery();
    jcf.replaceAll();
    initAnhor();

    if ($(window).innerWidth() > 812 ) {
        svgScale('M0,0 %inner%,130 v590 L0,832 z', '#mask');
    }
    else {
        svgScale('M0,0 %inner%,40 v470 L0,570 z', '#mask');
    }

    // APPEAR
    window.addEventListener('resize', function (event) {
        if (window.innerWidth > 768) {
            appear();
            window.addEventListener('scroll', appear);
        } else {
            window.removeEventListener('scroll', appear);
        }
    });
    if (window.innerWidth > 768) {
        appear();
        window.addEventListener('scroll', appear);
    } else {
        window.removeEventListener('scroll', appear);
    }
    // APPEAR END

    conversionImages();

    $('.parallax').parallax();

    $(".scroller").simplyScroll({
        speed: 2,
        pauseOnHover: false,
        pauseOnTouch: false,
        frameRate: 50
    });

    navFixedBtn();

});

function navFixedBtn() {

    window.addEventListener('scroll', function (event) {
        if (($(document).scrollTop() > 0) && (window.innerWidth <= 768)) {
            $('[data-btn-fixed]').addClass('fixed');
        } else {
            $('[data-btn-fixed]').removeClass('fixed');
        }

    });

}
function initAnhor() {

    if ( localStorage.anchor ) {

        $('html, body').animate(
            { scrollTop: $(localStorage.anchor).offset().top - $('#header').height() }, 1500
        );

        localStorage.clear();
    }

    $('.nav a[href ^= "#"]').click(function (e) {

        let $id = $(this).attr('href');
        let $target = $($id).offset().top;

        if ( $($id).length === 1) {

            localStorage.clear();

            e.preventDefault();

            $('body').toggleClass('menu-opened');
            $('html, body').animate(
                { scrollTop: $target - $('#header').height() }, 1500
            );
        } else {
            localStorage.anchor = $id;
            window.location.replace(window.location.origin);
        }
    });

    var G_Height_1000 = 130,
        G_Height_300 = 0,
        G_Height_600 = 90;
    $('.swiper-slide[href ^= "#"]').click(function (e) {

        let $id = $(this).attr('href');
        let $target = $($id).offset().top;

        if ( $($id).length === 1) {

            if (window.innerWidth > 1000) var _w = G_Height_1000;
            else if (window.innerWidth > 768) var _w = G_Height_600;
            else var _w = G_Height_300;

            localStorage.clear();

            e.preventDefault();
            if ($id === '#anhor-slide-3') {
                $('html, body').animate(
                    { scrollTop: $target - $('#header').height() - _w }, 1500
                );
            } else {
                $('html, body').animate(
                    { scrollTop: $target - $('#header').height() }, 1500
                );
            }

        } else {
            localStorage.anchor = $id;
            window.location.replace(window.location.origin);
        }
    });

    //anchor form

    //let $anchor = $('.nav a[href *= "#"]');

    //let $form = $('#contact-form');
    //let $target = $('.only').offset().top + $('.only').outerHeight();

    //if (localStorage.anchor) {
    //	$('html, body').animate(
    //		{ scrollTop: $target }, 1000
    //	);
    //}
    //
    //if ( $form.length === 1 ) {
    //	localStorage.anchor = null;
    //
    //	$anchor.click( function(e) {
    //		e.preventDefault();
    //		$('html, body').animate(
    //			{ scrollTop: $target }, 1000
    //		);
    //	});
    //} else {
    //	$anchor.click( function() {
    //		localStorage.anchor = '#contact-form';
    //	});
    //}
}

$(window).on('resize', function() {
    if ($(window).innerWidth() > 812 ) {
        svgScale('M0,0 %inner%,130 v590 L0,832 z', '#mask');
    }
    else {
        svgScale('M0,0 %inner%,40 v470 L0,570 z', '#mask');
    }
});

$(window).on('scroll', function() {
    if ( !$('body').hasClass('menu-opened') && $(window).scrollTop() > 70 ) {
        $('a.open-menu.fixed').show();
    }
});

function svgScale(pattern, sel) {
    pattern = pattern.replace('%inner%', 'l' + $(window).innerWidth());
    $(sel + ' path').attr('d', pattern);
}

function mobileMenu(){
    $('.open-menu, .close').click(function(){
        if ( !$('body').hasClass('menu-opened') && $(window).scrollTop() < 70 ) {
            $('a.open-menu.fixed').hide();
        } else {
            $('a.open-menu.fixed').show();
        }
        $('body').toggleClass('menu-opened');
        return false;
    });
}
var allGallery = {};
function swiperGallery(){
    var swiper = new Swiper('.swiper-container', {
        spaceBetween: 30,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
}
function appear() {	$('.appear').each(function() {
    if ( $(this).is(':in-viewport') && !$(this).hasClass('animated')) {
        $(this).addClass('animated');
    } else if( !$(this).is(':in-viewport') && $(this).hasClass('animated')){
        $(this).removeClass('animated');
    }
});
}

function conversionImages(){
    $('.bg').each(function() {
        $(this).css('background-image', 'url(' + $(this).find('> img').attr('src') + ')').find('> img').hide();
    });
}

(function ($) {

    $.fn.parallax = function () {
        var window_width = $(window).width();
        // Parallax Scripts
        return this.each(function(i) {
            var $this = $(this);
            $this.addClass('parallax');

            function updateParallax(initial) {
                var container_height;
                if (window_width < 601) {
                    container_height = ($this.height() > 0) ? $this.height() : $this.children("img").height();
                }
                else {
                    container_height = ($this.height() > 0) ? $this.height() : 500;
                }
                var $img = $this.children("img").first();
                var img_height = $img.height();
                var parallax_dist = img_height - container_height;
                var bottom = $this.offset().top + container_height;
                var top = $this.offset().top;
                var scrollTop = $(window).scrollTop();
                var windowHeight = window.innerHeight;
                var windowBottom = scrollTop + windowHeight;
                var percentScrolled = (windowBottom - top) / (container_height + windowHeight);
                var parallax = Math.round((parallax_dist * percentScrolled));

                if (initial) {
                    $img.css('display', 'block');
                }
                if ((bottom > scrollTop) && (top < (scrollTop + windowHeight))) {
                    $img.css('transform', "translate3D(-50%," + parallax*4 + "px, 0)");
                }

            }

            // Wait for image load
            $this.children("img").one("load", function() {
                updateParallax(true);
            }).each(function() {
                if (this.complete) $(this).trigger("load");
            });

            $(window).scroll(function() {
                window_width = $(window).width();
                updateParallax(false);
            });

            $(window).resize(function() {
                window_width = $(window).width();
                updateParallax(false);
            });

        });

    };
}( jQuery ));

function joinErrorValidation() {

    var _ = {};

    if (!document.querySelector('#company_form_send')) return _.stage = false;
    else _.stage = true;

    var _massInputs = document.querySelectorAll('#company_form_send .input-text'),
        _massSelects = document.querySelectorAll('.area-box select');

    var btnSbm = document.querySelector('#company_form_send #submit');

    var company_form_send = document.querySelector('#company_form_send');

    for (var i = 0; i < _massInputs.length; i++) {
        _massInputs[i].addEventListener('change', function (event) {
            _.clearError();
        })
    }

    for (var i = 0; i < _massSelects.length; i++) {
        _massSelects[i].addEventListener('change', function (event) {
            _.clearError();
            jcf.replaceAll();
        })
    }

    btnSbm.addEventListener('click', function (event) {
        if (_.validateCustom()) {
            return true;
        } else {
            event.preventDefault();
            return false;
        }
        jcf.replaceAll();
    })

    _.clearError = function () {

        for (var i = 0; i < _massInputs.length; i++) {
            if (_massInputs[i].classList.contains('error')) _massInputs[i].classList.remove('error');
        }

        for (var i = 0; i < _massSelects.length; i++) {
            if (_massSelects[i].classList.contains('error')) _massSelects[i].classList.remove('error');
            if (_massSelects[i].nextElementSibling.classList.contains('error')) _massSelects[i].nextElementSibling.classList.remove('error');
        }


    };

    _.validateCustom = function () {

        for (var i = 0; i < _massInputs.length; i++) {
            if (_massInputs[i].value.length === 0) _massInputs[i].classList.add('error');
        }

        for (var i = 0; i < _massSelects.length; i++) {
            if (_massSelects[i].value === "") {
                _massSelects[i].classList.add('error');
                if (!_massSelects[i].nextElementSibling.classList.contains('error')) _massSelects[i].nextElementSibling.classList.add('error');
            }
        }

        if (document.querySelectorAll('#company_form_send .error').length > 0) return false;
        else return true;

    };


    return _;

}