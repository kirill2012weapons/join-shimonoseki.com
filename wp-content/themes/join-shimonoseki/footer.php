            <footer class="footer">
                <section class="holder">
                    <div class="info">
                        <div class="col">
                            <h1 class="logo"><a href="/">Join しよう!</a></h1>
                            <p>下関市 若者就職支援事業</p>
                            <p>まちなかのキャリアセンター</p>
                        </div>
                        <div class="col">
                            <p>＜運営事務局＞ </p>
                            <p>株式会社ザメディアジョン・リージョナル <br />
                                〒750-0007 下関市赤間町1-10 <br />
                                創業支援カフェ KARASTA.内</p>
                        </div>
                    </div>
                    <div class="sub-info">
                        <p>TEL. <a class="tel" href="tel:0832274747">083-227-4747</a></p>
                        <p>10:00〜19:00（金土〜20:00／水曜休）</p>
                        <p><a href="mailto:join@tmr-inc.jp">join@tmr-inc.jp</a></p>
                    </div>
                </section>
                <p class="copy">&copy;︎ The Mediasion Regional Co.,Ltd  All Rights Reserved.</p>
            </footer>
            <a href="/join" class="btn-join-bottom">Join</a>
        </div>
    <?php wp_footer(); ?>
	<!-- Yahoo Code for your Target List -->
	<script type="text/javascript" language="javascript">
	/* <![CDATA[ */
	var yahoo_retargeting_id = 'YB3ETQB4E7';
	var yahoo_retargeting_label = '';
	var yahoo_retargeting_page_type = '';
	var yahoo_retargeting_items = [{item_id: '', category_id: '', price: '', quantity: ''}];
	/* ]]> */
	</script>
	<script type="text/javascript" language="javascript" src="https://b92.yahoo.co.jp/js/s_retargeting.js"></script>

    </body>
</html>