<?php
if ($_POST['upflg'] == 1) {

    mb_language("Japanese");
    mb_internal_encoding("UTF-8");

    $mail_title = "Joinしよう！運営事務局";
    $mailto = get_option('admin_email');
    $site_url_mail = get_option('siteurl');
    $header_bcc = "join@tmr-inc.jp";
    $subject = "Joinしよう！運営事務局でございます。";

    $message = <<<_message
{$_POST['uname']}

Joinしよう！運営事務局でございます。

お問い合わせありがとうございます。

以下の内容で受け付けました。
-----------------------------------------
イベント：
【学生】
{$_POST['checkboxGroupOne'][0]}
{$_POST['checkboxGroupOne'][1]}
【企業】
{$_POST['checkboxGroupTwo'][0]}
{$_POST['checkboxGroupTwo'][1]}
お名前： {$_POST['uname']}
生年月日： {$_POST['date-year']}-{$_POST['date-month']}-{$_POST['date-day']}
メールアドレス： {$_POST['email']}
所属（学生の方は、学校名と学年／企業の方は、社名と役職または部署）： {$_POST['joining']}
電話番号： {$_POST['phone']}
内容： {$_POST['message']}
-----------------------------------------

通常1～2営業日以内にご返信させていただきます。
ご返信まで今しばらくお待ち下さいませ。

万が一、1週間以上たっても担当より返信がない場合は
回答が迷惑メールフォルダに入っているか、ご登録の
メールアドレスが間違っている可能性が御座いますので
ご確認をお願い致します。

それでも返答が見つからない場合、お手数をお掛け致しますが、
再度お問い合わせ下さいますようお願い申し上げます。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

【このメールにお心当たりのない方】
お心当たりのない場合は、このまま削除いただきますようにお願いいたします。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

Joinしよう！運営事務局（KARASTA.内）　 https://join-shimonoseki.com/
TEL 083-227-4747　営業時間 10:00～19:00（金土〜20:00／水曜休）
MAIL join@tmr-inc.jp

運営：
株式会社ザメディアジョン・リージョナル
〒750-0007 下関市赤間町1-10 創業支援カフェ KARASTA.内
TEL 083-227-4747
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
_message;

    $fromName = mb_encode_mimeheader('Joinしよう！運営事務局');
    $header = "From:{$fromName} <no-reply@join-shimonoseki.com>" .PHP_EOL;
    $header .= "Bcc:{$header_bcc}";

    mb_send_mail($mailto, $subject, $message, $header);

    $header = "From:{$fromName} <{$mailto}>" .PHP_EOL;
    $mailto_guest = $_POST['email'];

    mb_send_mail($mailto_guest,$subject,$message,$header);

    wp_redirect( '/thanks' );
}

?>
<?php get_header(); ?>
<section class="top-section">
    <h2 class="title-block"><?= single_post_title(); ?></h2>
    <svg class="svg02" version="1.1"  width="100%" height="100px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="0 0 100 100" preserveAspectRatio="none" >
        <polygon fill="#eee" points="0,100 100,0 100,100"/>
    </svg>
    <div class="scroller">
        <div><img src="<?= ASSETS ?>images/img-top-2.jpg" alt="image 1"></div>
    </div>
</section>
<section class="join-section">
    <h2>各種イベントへの参加エントリーや<br class="sp" />ご質問・ご相談など <br />お気軽にお問い合わせください。</h2>
    <form class="form-box" method="POST" action="?" id="company_form_send">
        <div class="box">
            <h3>直近の各イベントのエントリー・ご質問の場合は、チェックを入れてください。（複数可）</h3>
            <h4>【学生・一般求職者・見学希望者】</h4>
            <div class="row">
						<label>
                    <input class="input-checkbox" type="checkbox" name="checkboxGroupOne[0]" value="２０１９年２月１３日　しものせき企業博 第一部" />
							<span class="area">2019年2月13日(水)　しものせき企業博 第一部（面接会）</span>
                </label>
						<label>
							<input class="input-checkbox" type="checkbox" name="checkboxGroupOne[1]" value="２０１９年２月１３日　しものせき企業博 第二部" />
							<span class="area">2019年2月13日(水)　しものせき企業博 第二部（業界研究会）</span>
                </label>
            </div>
            <h4>【企業】</h4>
            <div class="row">
                <label>
                    <input class="input-checkbox" type="checkbox" name="checkboxGroupTwo[0]" value="２０１９年２月１３日　しものせき企業博 第一部" />
					<span class="area">2019年2月13日(水)　しものせき企業博 第一部【面接会／企業受付終了】</span>
                </label>						
                <label>
							<input class="input-checkbox" type="checkbox" name="checkboxGroupTwo[1]" value="２０１９年２月１３日　しものせき企業博 第二部" />
							<span class="area">2019年2月13日(水)　しものせき企業博 第二部【業界研究会／企業受付終了】</span>
                </label>
            </div>
        </div>
        <div class="join-form">
            <dl>
                <dt><span>お名前<i>必須</i></span></dt>
                <dd>
                    <input class="input-text" type="text" value="" name="uname" />
                </dd>
            </dl>
            <dl>
                <dt><span>生年月日<i>必須</i></span></dt>
                <dd>
                    <ul class="area-box">
                        <li>
                            <select data-jcf="{&quot;wrapNative&quot;: false, &quot;wrapNativeOnMobile&quot;: false, &quot;useCustomScroll&quot;: false, &quot;multipleCompactStyle&quot;: true}" name="date-year" class="hidden-select" id="date-year">
                                <option value=""></option>
                                <option value="1930">1930</option>
                                <option value="1931">1931</option>
                                <option value="1932">1932</option>
                                <option value="1933">1933</option>
                                <option value="1934">1934</option>
                                <option value="1935">1935</option>
                                <option value="1936">1936</option>
                                <option value="1937">1937</option>
                                <option value="1938">1938</option>
                                <option value="1939">1939</option>
                                <option value="1940">1940</option>
                                <option value="1941">1941</option>
                                <option value="1942">1942</option>
                                <option value="1943">1943</option>
                                <option value="1944">1944</option>
                                <option value="1945">1945</option>
                                <option value="1946">1946</option>
                                <option value="1947">1947</option>
                                <option value="1948">1948</option>
                                <option value="1949">1949</option>
                                <option value="1950">1950</option>
                                <option value="1951">1951</option>
                                <option value="1952">1952</option>
                                <option value="1953">1953</option>
                                <option value="1954">1954</option>
                                <option value="1955">1955</option>
                                <option value="1956">1956</option>
                                <option value="1957">1957</option>
                                <option value="1958">1958</option>
                                <option value="1959">1959</option>
                                <option value="1960">1960</option>
                                <option value="1961">1961</option>
                                <option value="1962">1962</option>
                                <option value="1963">1963</option>
                                <option value="1964">1964</option>
                                <option value="1965">1965</option>
                                <option value="1966">1966</option>
                                <option value="1967">1967</option>
                                <option value="1968">1968</option>
                                <option value="1969">1969</option>
                                <option value="1970">1970</option>
                                <option value="1971">1971</option>
                                <option value="1972">1972</option>
                                <option value="1973">1973</option>
                                <option value="1974">1974</option>
                                <option value="1975">1975</option>
                                <option value="1976">1976</option>
                                <option value="1977">1977</option>
                                <option value="1978">1978</option>
                                <option value="1979">1979</option>
                                <option value="1980">1980</option>
                                <option value="1981">1981</option>
                                <option value="1982">1982</option>
                                <option value="1983">1983</option>
                                <option value="1984">1984</option>
                                <option value="1985">1985</option>
                                <option value="1986">1986</option>
                                <option value="1987">1987</option>
                                <option value="1988">1988</option>
                                <option value="1989">1989</option>
                                <option value="1990">1990</option>
                                <option value="1991">1991</option>
                                <option value="1992">1992</option>
                                <option value="1993">1993</option>
                                <option value="1994">1994</option>
                                <option value="1995">1995</option>
                                <option value="1996">1996</option>
                                <option value="1997">1997</option>
                                <option value="1998">1998</option>
                                <option value="1999">1999</option>
                                <option value="2000">2000</option>
                                <option value="2001">2001</option>
                                <option value="2002">2002</option>
                                <option value="2003">2003</option>
                                <option value="2004">2004</option>
                                <option value="2005">2005</option>
                                <option value="2006">2006</option>
                                <option value="2007">2007</option>
                                <option value="2008">2008</option>
                                <option value="2009">2009</option>
                                <option value="2010">2010</option>
                                <option value="2011">2011</option>
                                <option value="2012">2012</option>
                                <option value="2013">2013</option>
                                <option value="2014">2014</option>
                                <option value="2015">2015</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                            </select>
                        </li>
                        <li>
                            <select data-jcf="{&quot;wrapNative&quot;: false, &quot;wrapNativeOnMobile&quot;: false, &quot;useCustomScroll&quot;: false, &quot;multipleCompactStyle&quot;: true}" name="date-month" class="hidden-select" id="date-month">
                                <option value=""></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                        </li>
                        <li>
                            <select data-jcf="{&quot;wrapNative&quot;: false, &quot;wrapNativeOnMobile&quot;: false, &quot;useCustomScroll&quot;: false, &quot;multipleCompactStyle&quot;: true}" name="date-day" class="hidden-select" id="date-day">
                                <option value=""></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                        </li>
                    </ul>
                </dd>
            </dl>
            <dl>
                <dt><span>メールアドレス<i>必須</i></span></dt>
                <dd><input class="input-text" type="text" value="" name="email" /></dd>
            </dl>
            <dl>
                <dt><span>所属（学生の方は、学校名と学年／企業の方は、社名と役職または部署）<i>必須</i></span></dt>
                <dd><input class="input-text" type="text" value="" name="joining" /></dd>
            </dl>
            <dl>
                <dt><span>電話番号<i>必須</i></span></dt>
                <dd><input class="input-text" type="text" value="" name="phone" /></dd>
            </dl>
            <dl>
                <dt><span>お問い合わせ内容<i>必須</i></span></dt>
                <dd>
                    <textarea class="input-text" rows="5" cols="30" placeholder="8月17日の就職面接会に参加希望です。" name="message"></textarea>
                </dd>
            </dl>
        </div>
        <p>※必要情報を入力いただき、 <br class="sp" />「<a href="https://www.karasta.jp/privacy">プライバシーポリシー</a>」に同意いただいた上で、送信ボタンを押してください。</p>
        <input type="hidden" name="upflg" value="1">
        <button class="btn-send" id="submit" type="submit"><span>send</span></button>
    </form>
</section>
<?php get_footer(); ?>
