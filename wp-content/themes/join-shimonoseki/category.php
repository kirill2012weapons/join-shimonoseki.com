<?php get_header(); ?>

<?php

$page_object = get_queried_object();

if (isset($_GET['join'])) {

    $postID = $_GET['join'];

} else {

    /**
     * TODO СДЕЛАТЬ ИСКЛЮЧЕНИЕ СКА
     */
    $page_object = get_queried_object();
    $postID = $_GET['join'];

}



$categoryObjArr = get_the_category($postID);

$queryPost = new WP_Query([
        'p' => $postID,
]);

$currPost = get_post($postID);

$prevQUERY = "
SELECT p.ID 
FROM wp_posts AS p 
INNER JOIN wp_term_relationships AS tr 
ON p.ID = tr.object_id 
INNER JOIN wp_term_taxonomy tt 
ON tr.term_taxonomy_id = tt.term_taxonomy_id 
WHERE p.post_date < '" . $currPost->post_date . "' 
AND p.post_type = 'post' 
AND tt.taxonomy = 'category' 
AND tt.term_id = " . $page_object->term_taxonomy_id . " 
AND p.post_status = 'publish' 
ORDER BY p.post_date DESC 
LIMIT 1";

$nextQUERY = "
SELECT p.ID 
FROM wp_posts AS p 
INNER JOIN wp_term_relationships AS tr 
ON p.ID = tr.object_id 
INNER JOIN wp_term_taxonomy tt 
ON tr.term_taxonomy_id = tt.term_taxonomy_id 
WHERE p.post_date > '" . $currPost->post_date . "' 
AND p.post_type = 'post' 
AND tt.taxonomy = 'category' 
AND tt.term_id = " . $page_object->term_taxonomy_id . " 
AND p.post_status = 'publish' 
ORDER BY p.post_date ASC 
LIMIT 1";

global $wpdb;

$prevQUERY_results = $wpdb->get_results( $prevQUERY, OBJECT );
$nextQUERY_results = $wpdb->get_results( $nextQUERY, OBJECT );

//d('PREV');
//d($prevQUERY_results);
//d('NEXT');
//d($nextQUERY_results);

?>

    <section class="top-section" data-category-php>
        <h2 class="title-block">NEWS</h2>
        <svg class="svg02" version="1.1"  width="100%" height="100px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="0 0 100 100" preserveAspectRatio="none" >
    <polygon fill="#fff" points="0,100 100,0 100,100"/>
</svg>
    <div class="scroller">
        <div><img src="<?= ASSETS ?>images/img-top-2.jpg" alt="image 1"></div>
    </div>
    </section>
    <section class="news-section">
        <div class="holder">
            <div class="two-colums">
                <section class="main">
<!--                        --><?php //if (have_posts()): while(have_posts()): the_post(); ?>
                    <?php if ($queryPost->have_posts()): while($queryPost->have_posts()): $queryPost->the_post(); ?>d
                        <div class="news-info">
                            <?= get_the_date('Y年n月j日'); ?>
                        </div>
                        <h2><?php the_title(); ?></h2>
                        <?php the_content(); ?>
                        <div class="btn-next">

                            <?php

                            $nextPost = get_previous_post(true, '', 'category');
                            $prevPosts = get_next_post(true, '', 'category');

                            ?>

                            <?php if (!empty($nextQUERY_results)) : ?>
                                <a class="btn-next-prev" href="<?= add_query_arg('join', $nextQUERY_results[0]->ID, $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']) ?>">PREV</a>
                            <?php endif; ?>

                            <?php
                                echo '<a class="btn-next-next btn-news" href="https://join-shimonoseki.com/news">NEWS 一覧へ</a>';
                            ?>

                            <?php if (!empty($prevQUERY_results)) : ?>
                                <a class="btn-next-next" href="<?= add_query_arg('join', $prevQUERY_results[0]->ID, $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']) ?>">NEXT</a>
                            <?php endif; ?>

<!--                            --><?php
//                            $previous_posts_link = get_previous_posts_link('PREV');
//                            $next_posts_link = get_next_posts_link('NEXT');
//                            if ($previous_posts_link) echo substr_replace($previous_posts_link, 'class="btn-next-prev"', 3, 0);
//
//                            echo '<a class="btn-next-next btn-news" href="https://join-shimonoseki.com/news">一覧</a>';
//
//                            if ($next_posts_link) echo substr_replace($next_posts_link, 'class="btn-next-next"', 3, 0);
//                            ?>
                        </div>
                    <?php endwhile; endif; wp_reset_postdata(); ?>
                </section>
                <?php get_sidebar(); ?>
            </div>
        </div>
    </section>
<?php get_footer(); ?>