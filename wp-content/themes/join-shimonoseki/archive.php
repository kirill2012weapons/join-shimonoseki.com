<?php get_header(); ?>
    <section class="top-section" data-archive-php>
        <h2 class="title-block">NEWS</h2>
        <svg class="svg02" version="1.1"  width="100%" height="100px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="0 0 100 100" preserveAspectRatio="none" >
        <polygon fill="#fff" points="0,100 100,0 100,100"/>
    </svg>
    <div class="scroller">
        <div><img src="<?= ASSETS ?>images/img-top-2.jpg" alt="image 1"></div>
    </div>
    </section>
    <section class="news-section">
        <div class="holder">
            <div class="two-colums">
                <section class="main">
                    <?php if (have_posts()): while(have_posts()): the_post(); ?>
                        <div class="news-info">
                            <?= get_the_date('Y年n月j日'); ?>
                            <?php $categories = get_the_category(); ?>
                            <?php foreach ($categories as $category): ?>
                                <a href="<?= get_category_link($category->cat_ID); ?>"><?= $category->name; ?></a>
                            <?php endforeach; ?>
                        </div>
                        <h2><?php the_title(); ?></h2>
                        <?php the_content(); ?>
                        <div class="btn-next">
                            <?php
                            $previous_posts_link = get_previous_posts_link('PREV');
                            $next_posts_link = get_next_posts_link('NEXT');
                            if ($previous_posts_link) echo substr_replace($previous_posts_link, 'class="btn-next-prev"', 3, 0);

                            echo '<a class="btn-next-next btn-news" href="https://join-shimonoseki.com/news">NEWS 一覧へ</a>';

                            if ($next_posts_link) echo substr_replace($next_posts_link, 'class="btn-next-next"', 3, 0);
                            ?>
                        </div>
                    <?php endwhile; endif; wp_reset_postdata(); ?>
                </section>
                <?php get_sidebar(); ?>
            </div>
        </div>
    </section>
<?php get_footer(); ?>