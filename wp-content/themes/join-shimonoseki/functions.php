<?php

define('ASSETS', get_template_directory_uri() . '/assets/');

function enqueue_assets()
{
    wp_enqueue_style('main_style', ASSETS . 'css/main.css', false);
    wp_enqueue_style('main_style_addon', ASSETS . 'css/addon.css', false);
    wp_enqueue_style('roboto', 'https://fonts.googleapis.com/css?family=Roboto:300,300i,400,500,500i,700,700i,900,900i', false);

    wp_enqueue_script('jquery_min_js', ASSETS . 'js/jquery.min.js', false);
    wp_enqueue_script('jquery_migrate_js', ASSETS . 'js/jquery-migrate.js', false);
    wp_enqueue_script('isInViewport_min_js', ASSETS . 'js/isInViewport.min.js', false);
    wp_enqueue_script('swiper_min_js', ASSETS . 'js/swiper.min.js', false);
    wp_enqueue_script('slider_min_js', ASSETS . 'js/slider.min.js', false);
    wp_enqueue_script('jcf_js', ASSETS . 'js/jcf.js', false);
    wp_enqueue_script('jcf_select_js', ASSETS . 'js/jcf.select.js', false);
    wp_enqueue_script('main_js', ASSETS . 'js/main.js', false);
}
add_action('wp_enqueue_scripts', 'enqueue_assets');

/**
 * Disable the emoji's
 */
function disable_emojis() {
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
    add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
    if ( 'dns-prefetch' == $relation_type ) {
        /** This filter is documented in wp-includes/formatting.php */
        $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

        $urls = array_diff( $urls, array( $emoji_svg_url ) );
    }

    return $urls;
}

function d($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

function true_301_redirect() {
    /* в массиве указываем все старые=>новые ссылки  */
    $rules = array(
        array('old'=>'/2018','new'=>'/news?y=2018'),
        array('old'=>'/2019','new'=>'/news?y=2019'),
        array('old'=>'/2018/','new'=>'/news?y=2018'),
        array('old'=>'/2019/','new'=>'/news?y=2019'),
    );
    foreach( $rules as $rule ) :
        // если URL совпадает с одним из указанных в массиве, то редиректим
        if( urldecode($_SERVER['REQUEST_URI']) == $rule['old'] ) :
            wp_redirect( site_url( $rule['new'] ), 301 );
            exit();
        endif;
    endforeach;
}

add_action('template_redirect', 'true_301_redirect');

add_filter( 'post_thumbnail_html', 'remove_wps_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_wps_width_attribute', 10 );
add_filter( 'the_content', 'remove_wps_width_attribute', 10 );

function remove_wps_width_attribute( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}