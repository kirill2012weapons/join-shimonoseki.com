<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta name="description" content="山口県下関市の若者地元就職支援事業「Joinしよう！」。新たな取り組みで学校と企業、行政が連携し就活における相互のマッチングをサポート。「まちなかのキャリアセンター（karasta.【カラスタ】内）」が運営を行います。下関に「Joinしよう！」。">
    <meta name="keywords" content="山口県下関市の若者地元就職支援事業「Joinしよう！」。新たな取り組みで学校と企業、行政が連携し就活における相互のマッチングをサポート。「まちなかのキャリアセンター（karasta.【カラスタ】内）」が運営を行います。下関に「Joinしよう！」。">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Join しよう!">
    <meta property="og:title" content="Joinしよう！｜下関市 若者の地元就職支援事業">
    <meta property="og:description" content="山口県下関市の若者地元就職支援事業「Joinしよう！」。新たな取り組みで学校と企業、行政が連携し就活における相互のマッチングをサポート。「まちなかのキャリアセンター（karasta.【カラスタ】内）」が運営を行います。下関に「Joinしよう！」。">
    <meta property="og:url" content="<?= get_option('siteurl'); ?>">
    <meta property="og:locale" content="ja_JA">
    <meta property="og:image" content="<?= ASSETS ?>images/ogp.png">
    <meta property="og:image:width" content="968">
    <meta property="og:image:height" content="504">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Joinしよう！｜下関市 若者の地元就職支援事業">
    <meta name="twitter:description" content="山口県下関市の若者地元就職支援事業「Joinしよう！」。新たな取り組みで学校と企業、行政が連携し就活における相互のマッチングをサポート。「まちなかのキャリアセンター（karasta.【カラスタ】内）」が運営を行います。下関に「Joinしよう！」。">
    <meta name="twitter:image:src" content="<?= ASSETS ?>images/ogp.png">
    <meta name="twitter:url" content="<?= get_option('siteurl'); ?>">
    <meta name="twitter:domain" content="<?= get_option('siteurl'); ?>">
    <link rel="icon" type="image/x-icon" href="<?= ASSETS ?>images/favicon.ico">
    <link rel="apple-touch-icon" href="<?= ASSETS ?>images/apple-touch-icon152.png" sizes="152x152">
    <meta name="theme-color" content="#fff">
    <meta name="msapplication-navbutton-color" content="#fff">
    <meta name="apple-mobile-web-app-status-bar-style" content="#fff">
    <title>Joinしよう！｜下関市 若者の地元就職支援事業</title>
		<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NZMSJJR');</script>
<!-- End Google Tag Manager -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-118033982-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-118033982-2');
    </script>
    <?php wp_head(); ?>
	<!-- Global site tag (gtag.js) - Google Ads: 806187936 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-806187936"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());
		
  		gtag('config', 'AW-806187936');
	</script>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NZMSJJR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="wrapper"
    <?php
        if (get_query_var('pagename') === 'join') echo 'class="join-grey"'
    ?>>
    <header id="header" class="header">
        <section class="header-holder">
            <h1 class="logo"><a href="/">Join しよう!</a></h1>
            <strong class="slogan">若者の地元就職支援事業</strong>
            <a href="#" data-btn-fixed class="open-menu"><span>open menu</span></a>
        </section>
        <a href="/join" class="btn-join"><span>Join</span></a>
        <nav class="nav">
            <ul>
                <li><a href="#anhor01"><span>ABOUT PROJECT</span></a></li>
                <li><a href="#anhor02"><span>EVENT</span></a></li>
                <li><a href="#anhor03"><span>CONTENTS</span></a></li>
                <li><a href="#anhor04"><span>PRODUCT</span></a></li>
                <li><a href="#anhor05"><span>NEXT PROJECT</span></a></li>
                <li class="link"><a href="#anhor06"><span>FACILITY</span></a></li>
                <li class="link"><a href="/news"><span>NEWS</span></a></li>
                <li class="link"><a href="/join"><span>JOIN</span></a></li>
            </ul>
        </nav>
    </header>